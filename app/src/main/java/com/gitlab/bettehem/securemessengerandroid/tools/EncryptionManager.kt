package com.gitlab.bettehem.securemessengerandroid.tools

import android.os.Build
import java.security.MessageDigest
import org.apache.commons.codec.android.binary.Base64
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

//this class is used for encrypting and decrypting
class EncryptionManager {

    /**
     * Creates an sha-256 hash of given text.
     * @param secretText the text that needs to be encrypted
     * @return returns an sha-256 hash with url safe Base64 encoding
     */
    fun createHash(secretText: String): String {
        val sha = MessageDigest.getInstance("SHA-256")
        val hash = sha.digest(sha.digest(sha.digest(sha.digest(secretText.toByteArray(Charsets.UTF_8).reversedArray()) + secretText.toByteArray(Charsets.UTF_8)) + secretText.toByteArray(Charsets.UTF_8).reversedArray()) + sha.digest(secretText.toByteArray(Charsets.UTF_8).reversedArray()))

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            java.util.Base64.getUrlEncoder().withoutPadding().encodeToString(hash)
        } else {
            Base64.encodeBase64URLSafeString(hash)
        }
    }

    /**
     * encrypts text with given key
     * @param secretKey a Base64 encoded key
     * @param secretText the secret text that should be encrypted
     * @return returns an EncryptedText object that holds an iv and the encrypted string
     */
    fun encrypt(secretKey: String, secretText: String): EncryptedText {
        //decode key
        val decodedKey = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            java.util.Base64.getUrlDecoder().decode(secretKey)
        } else {
            Base64.decodeBase64(secretKey)
        }
        //create key
        val key = SecretKeySpec(decodedKey, "AES")
        //initialize cipher
        val cipher: Cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
        cipher.init(Cipher.ENCRYPT_MODE, key)
        //encrypt string
        val encryptedString = cipher.doFinal(secretText.toByteArray(Charsets.UTF_8))
        //Base64 encode encrypted string and iv
        val encodedString: String
        val encodedIv: String
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            encodedString = java.util.Base64.getUrlEncoder().withoutPadding().encodeToString(encryptedString)
            encodedIv = java.util.Base64.getUrlEncoder().withoutPadding().encodeToString(cipher.iv)
        } else {
            encodedString = Base64.encodeBase64URLSafeString(encryptedString)
            encodedIv = Base64.encodeBase64URLSafeString(cipher.iv)
        }

        return EncryptedText(encodedIv, encodedString)
    }

    fun decrypt(encryptedText: EncryptedText, secretKey: String): String {
        //decode secretKey, initVector and encryptedString
        val decodedKey: ByteArray
        val decodedIv: ByteArray
        val decodedString: ByteArray
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            decodedKey = java.util.Base64.getUrlDecoder().decode(secretKey)
            decodedIv = java.util.Base64.getUrlDecoder().decode(encryptedText.initVector)
            decodedString = java.util.Base64.getUrlDecoder().decode(encryptedText.encryptedString)
        } else {
            decodedKey = Base64.decodeBase64(secretKey)
            decodedIv = Base64.decodeBase64(encryptedText.initVector)
            decodedString = Base64.decodeBase64(encryptedText.encryptedString)
        }

        //create key
        val key = SecretKeySpec(decodedKey, "AES")
        //initialize cipher
        val cipher: Cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
        cipher.init(Cipher.DECRYPT_MODE, key, IvParameterSpec(decodedIv))

        //return decrypted string
        return String(cipher.doFinal(decodedString))
    }

    //used when handling aes encrypted data
    class EncryptedText(val initVector: String, val encryptedString: String)
}