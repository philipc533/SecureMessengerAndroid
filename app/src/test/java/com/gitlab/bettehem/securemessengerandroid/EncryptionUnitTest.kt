package com.gitlab.bettehem.securemessengerandroid

import com.gitlab.bettehem.securemessengerandroid.tools.EncryptionManager
import org.junit.Assert.assertEquals
import org.junit.Test

//this class is used to test EncryptionManager's functions
class EncryptionUnitTest{

    //tests the createHash function
    @Test fun hashTest(){
        val result = EncryptionManager().createHash("test")
        assertEquals("n4bQgYhMfWWaL-qgxVrQFaO_TxsrC4Is0V1sFbDwCgg", result)
    }

    //tests the encryption and decryption
    @Test fun encryption(){
        val passwd = "secret"
        //create hash from text "secret"
        println("Creating hash from text \"$passwd\"")
        val hash = EncryptionManager().createHash(passwd)
        println("Hash from \"$passwd\": $hash\n")
        //encrypt "test" with hash as the key
        val message = "test"
        println("Encrypting message \"$message\"")
        val encryptedText = EncryptionManager().encrypt(hash, message)
        println("InitVector: ${encryptedText.initVector}")
        println("Encrypted text: ${encryptedText.encryptedString}\n")
        //decrypt the encryptedText with hash as the key
        println("Decrypting encrypted text \"${encryptedText.encryptedString}\"")
        val decryptedText = EncryptionManager().decrypt(encryptedText, hash)
        println("Decrypted text: $decryptedText")
        assertEquals(message, decryptedText)
    }
}