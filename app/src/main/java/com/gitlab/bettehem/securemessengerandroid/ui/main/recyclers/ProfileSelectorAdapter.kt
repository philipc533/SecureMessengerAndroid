package com.gitlab.bettehem.securemessengerandroid.ui.main.recyclers

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gitlab.bettehem.securemessengerandroid.R
import com.gitlab.bettehem.securemessengerandroid.tools.ProfileManager

class ProfileSelectorAdapter(val activity: FragmentActivity, val profileSelectorListener: ProfileSelectorListener): RecyclerView.Adapter<ProfileSelectorViewHolder>() {

    private lateinit var profileItems: ArrayList<ProfileManager.UserProfile>
    private var checkBoxVisibility = View.GONE


    fun setProfileItems(items: ArrayList<ProfileManager.UserProfile>){
        profileItems = items
        notifyDataSetChanged()
    }

    fun updateItem(item: ProfileManager.UserProfile){
        for (i in profileItems.indices){
            if (profileItems[i].toString() == item.toString()){
                profileItems[i] = item
                notifyItemChanged(i)
            }
        }
    }

    fun removeItem(item: ProfileManager.UserProfile){
        //get items position
        var position = -1
        for (i in profileItems.indices){
            if (profileItems[i].toString() == item.toString()){
                position = i
            }
        }
        //if the item position was found, remove item
        if (position != -1){
            profileItems.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun setCheckBoxVisible(isVisible: Boolean){
        checkBoxVisibility = if (isVisible){
            View.VISIBLE
        }else{
            View.GONE
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfileSelectorViewHolder {
        return ProfileSelectorViewHolder(LayoutInflater.from(activity).inflate(R.layout.profile_selector_item_layout, parent, false), profileSelectorListener)
    }

    override fun getItemCount(): Int {
        return profileItems.size
    }

    override fun onBindViewHolder(holder: ProfileSelectorViewHolder, position: Int) {
        holder.usernameTextView?.text = activity.resources.getString(R.string.usernameText, profileItems[position].username)
        holder.userIdTextView?.text = activity.resources.getString(R.string.userIdText, profileItems[position].userID)
        holder.statusTextView?.text = activity.resources.getString(R.string.statusText, profileItems[position].status)
        holder.checkBox?.visibility = checkBoxVisibility
        holder.userProfile = profileItems[position]
    }

}