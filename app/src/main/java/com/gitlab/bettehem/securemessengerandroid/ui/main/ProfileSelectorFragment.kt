package com.gitlab.bettehem.securemessengerandroid.ui.main


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.res.ColorStateList
import android.os.Build
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.ViewCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.github.bettehem.androidtools.dialog.CustomAlertDialog
import com.github.bettehem.androidtools.interfaces.DialogButtonsListener
import com.gitlab.bettehem.securemessengerandroid.R
import com.gitlab.bettehem.securemessengerandroid.tools.ProfileManager
import com.gitlab.bettehem.securemessengerandroid.ui.main.recyclers.ProfileSelectorAdapter
import com.gitlab.bettehem.securemessengerandroid.ui.main.recyclers.ProfileSelectorListener
import com.gitlab.bettehem.securemessengerandroid.ui.main.viewmodels.ProfileManagerViewModel
import kotlinx.android.synthetic.main.fragment_profile_selector.*

class ProfileSelectorFragment : Fragment(), ProfileSelectorListener, DialogButtonsListener {

    private lateinit var viewModel: ProfileManagerViewModel
    private lateinit var profileSelectorAdapter: ProfileSelectorAdapter
    private var deleteModeEnabled = false
    private val deletableProfiles: ArrayList<ProfileManager.UserProfile> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_selector, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //attach view model
        viewModel = ViewModelProviders.of(activity!!).get(ProfileManagerViewModel::class.java)

        //setup buttons
        buttons()

        //setup observers
        observers()

        //setup profile list
        profileListSetup(view)
    }

    override fun onResume() {
        super.onResume()
        //check the navigation drawer's item(s)
        activity!!.findViewById<NavigationView>(R.id.nav_view).menu.getItem(0).isChecked = true
    }

    //setup button(s)
    private fun buttons(){
        profileSelectorFab.setOnClickListener {
            //check if delete mode is enabled
            if (!deleteModeEnabled){
                //do only if delete mode isn't active
                findNavController().navigate(R.id.toProfileCreatorFragment)
            }else{
                //do only if delete mode is active

                //check if there are any profiles that need deletion
                if (deletableProfiles.isNotEmpty()){
                    //ask user to confirm deletion
                    CustomAlertDialog.make(
                            activity,
                            getString(R.string.warningText),
                            getString(R.string.profileDeletionWarningMessage),
                            false,
                            getString(R.string.deleteText),
                            getString(R.string.cancelText),
                            this,
                            "profileDeleteConfirmationDialog").show()
                }else{
                    //tell user that they haven't selected items for deletion
                    Toast.makeText(activity, getString(R.string.noProfilesSelectedForDeletion), Toast.LENGTH_LONG).show()
                }
            }
        }

        //if user presses and holds the button, show checkboxes and allow profile deletion
        profileSelectorFab.setOnLongClickListener {
            if (!deleteModeEnabled){
                setDeletionMode(true)
            }else{
                setDeletionMode(false)
            }
            //return true to indicate the click event as handled
            true
        }
    }

    //setup observers
    private fun observers(){
        //observer for profile deletion
        val profileDeletionObserver: Observer<ProfileManager.UserProfile> = Observer { it ->
            //update profileSelector list
            profileSelectorAdapter.removeItem(it!!)

            //disable delete mode
            setDeletionMode(false)
        }
        //observe profile deletions
        viewModel.getDeletedProfile().observe(this, profileDeletionObserver)
    }


    //setup the profile list
    private fun profileListSetup(view: View) {
        val recyclerView = view.findViewById<RecyclerView>(R.id.profileSelectorRecycler)
        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        profileSelectorAdapter = ProfileSelectorAdapter(activity!!, this)

        viewModel.getProfileList().observe(this, Observer { it ->
            run {
                recyclerView.layoutManager = layoutManager
                profileSelectorAdapter.setProfileItems(it!!)
                recyclerView.adapter = profileSelectorAdapter
            }
        })
    }

    //sets deletion mode as active or inactive
    private fun setDeletionMode(isActive: Boolean){
        if (isActive){
            //set checkboxes as visible
            profileSelectorAdapter.setCheckBoxVisible(true)
            //rotate fab 45 degrees
            val interpolator = OvershootInterpolator()
            ViewCompat.animate(profileSelectorFab).rotation(45.toFloat()).withLayer().setDuration(450).setInterpolator(interpolator).start()

            //change color to red
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                profileSelectorFab.backgroundTintList = resources.getColorStateList(R.color.colorAccent, activity?.theme)
            }else{
                @Suppress("DEPRECATION")
                profileSelectorFab.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.colorAccent))
            }
            //set delete mode as enabled
            deleteModeEnabled = true
        }else{
            //hide checkboxes
            profileSelectorAdapter.setCheckBoxVisible(false)

            //rotate fab back to 0 degrees
            val interpolator = OvershootInterpolator()
            ViewCompat.animate(profileSelectorFab).rotation(0.toFloat()).withLayer().setDuration(450).setInterpolator(interpolator).start()

            //set fab color back to blue
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                profileSelectorFab.backgroundTintList = resources.getColorStateList(R.color.colorPrimary, activity?.theme)
            }else{
                @Suppress("DEPRECATION")
                profileSelectorFab.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.colorPrimary))
            }

            //set delete mode as disabled
            deleteModeEnabled = false
        }
    }





    //do when user selects a profile from the list
    override fun onProfileSelected(userProfile: ProfileManager.UserProfile) {
        val action = ProfileSelectorFragmentDirections.toProfileEditorFragment(userProfile.username, userProfile.userID, userProfile.status)
        findNavController().navigate(action)
    }

    //do when user selects a checkbox from the list
    override fun onProfileItemChecked(userProfile: ProfileManager.UserProfile) {
        //add checked profile to deletableProfiles list
        deletableProfiles.add(userProfile)
    }

    //do when user deselects a checkbox from the list
    override fun onProfileItemUnchecked(userProfile: ProfileManager.UserProfile) {
        //remove unchecked profile from deletableProfiles list
        deletableProfiles.remove(userProfile)
    }

    //do when the positive button in a CustomAlertDialog is clicked
    override fun onPositiveButtonClicked(id: String?) {
        //check which dialog's button was clicked
        when (id){
            "profileDeleteConfirmationDialog" -> viewModel.deleteProfiles(deletableProfiles)
        }
    }

    //do when the negative button in a CustomAlertDialog is clicked
    override fun onNegativeButtonClicked(id: String?) {}

    //do when the neutral button in a CustomAlertDialog is clicked
    override fun onNeutralButtonClicked(id: String?) {}
}
