package com.gitlab.bettehem.securemessengerandroid.ui.main.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.gitlab.bettehem.securemessengerandroid.tools.ProfileManager

class ProfileManagerViewModel(application: Application): AndroidViewModel(application){
    private val profileAddedLiveData: MutableLiveData<ProfileManager.UserProfile> = MutableLiveData()
    private val profileUpdatedLiveData: MutableLiveData<ProfileManager.UserProfile> = MutableLiveData()
    private val currentProfileLiveData: MutableLiveData<ProfileManager.UserProfile> = MutableLiveData()
    private val profileListLiveData: MutableLiveData<ArrayList<ProfileManager.UserProfile>> = MutableLiveData()
    private val profileDeletedLiveData: MutableLiveData<ProfileManager.UserProfile> = MutableLiveData()


    //LiveData functions
    fun getAddedProfile(): MutableLiveData<ProfileManager.UserProfile> = profileAddedLiveData

    fun getUpdatedProfile(): MutableLiveData<ProfileManager.UserProfile> = profileUpdatedLiveData

    fun getCurrentProfile(): MutableLiveData<ProfileManager.UserProfile>{
        currentProfileLiveData.value = ProfileManager().getCurrentProfile(getApplication())
        return currentProfileLiveData
    }

    fun getProfileList(): MutableLiveData<ArrayList<ProfileManager.UserProfile>>{
        profileListLiveData.value = ProfileManager().getUserProfileList(getApplication())
        return profileListLiveData
    }

    fun getDeletedProfile(): MutableLiveData<ProfileManager.UserProfile> = profileDeletedLiveData

    //saves a new profile
    fun saveNewProfile(username: String){
        val userID = ProfileManager().generateUserID()
        val userProfile = ProfileManager.UserProfile(username, userID)
        ProfileManager().saveNewProfile(getApplication(), userProfile)
        setCurrentProfile(userProfile)
        profileAddedLiveData.value = userProfile
    }

    //updates a profile's details
    fun updateProfileDetails(userProfile: ProfileManager.UserProfile, newUsername: String = userProfile.username){
        val newProfile = ProfileManager().updateProfile(getApplication(), userProfile, newUsername)
        profileUpdatedLiveData.value = newProfile
    }

    //sets the current profile
    fun setCurrentProfile(userProfile: ProfileManager.UserProfile){
        ProfileManager().setCurrentProfile(getApplication(), userProfile)
        currentProfileLiveData.value = userProfile
    }

    //deletes given profiles
    fun deleteProfiles(userProfiles: ArrayList<ProfileManager.UserProfile>){
        for (profile in userProfiles){
            ProfileManager().deleteProfile(getApplication(), profile)
            profileDeletedLiveData.value = profile
            currentProfileLiveData.value = ProfileManager().getCurrentProfile(getApplication())
        }
    }
}