package com.gitlab.bettehem.securemessengerandroid

import com.gitlab.bettehem.securemessengerandroid.tools.ProfileManager
import org.junit.Test

class ProfileManagerUnitTest{

    //prints a list of 29 random generated user id's
    @Test fun userIdTest(){
        var i = 0
        while (i < 20){
            System.out.println(ProfileManager().generateUserID())
            i++
        }
    }
}