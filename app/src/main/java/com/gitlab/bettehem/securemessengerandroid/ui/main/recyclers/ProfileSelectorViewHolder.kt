package com.gitlab.bettehem.securemessengerandroid.ui.main.recyclers

import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.CheckBox
import com.gitlab.bettehem.securemessengerandroid.R
import com.gitlab.bettehem.securemessengerandroid.tools.ProfileManager

class ProfileSelectorViewHolder(itemView: View?, var profileSelectorListener: ProfileSelectorListener) : RecyclerView.ViewHolder(itemView!!), View.OnClickListener {
    lateinit var userProfile: ProfileManager.UserProfile
    val usernameTextView = itemView?.findViewById<AppCompatTextView>(R.id.profileSelectorUsernameTextView)
    val userIdTextView = itemView?.findViewById<AppCompatTextView>(R.id.profileSelectorIdTextView)
    val statusTextView = itemView?.findViewById<AppCompatTextView>(R.id.profileSelectorStatusTextView)
    val checkBox = itemView?.findViewById<CheckBox>(R.id.profileSelectorCheckBox)
    init {
        itemView?.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        //when the itemView is clicked, check if the checkbox is visible.
        //if so, set the checkbox as checked/unchecked instead of calling onProfileSelected
        if (checkBox?.visibility == View.VISIBLE){
            //check to see if the checkbox is already checked.
            //if so, uncheck it
            if (!checkBox.isChecked){
                checkBox.isChecked = true
                profileSelectorListener.onProfileItemChecked(userProfile)
            }else{
                checkBox.isChecked = false
                profileSelectorListener.onProfileItemUnchecked(userProfile)
            }
        }else{
            profileSelectorListener.onProfileSelected(userProfile)
        }
    }
}
